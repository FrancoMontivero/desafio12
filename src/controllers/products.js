"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeProduct = exports.updateProduct = exports.addProduct = exports.getProduct = exports.getProducts = exports.socketProducts = void 0;
let currentId = 0;
let products = [];
let refIo;
function socketProducts(io) {
    refIo = io;
    return function () { io.emit('products', products); };
}
exports.socketProducts = socketProducts;
function getProducts() { return products; }
exports.getProducts = getProducts;
;
function getProduct(id) {
    if (Number.isNaN(Number(id)))
        throw new Error("El id del producto debe ser un caracter numerico");
    else if (typeof id !== "number")
        id = parseInt(id);
    const product = products.find(e => id === e.id);
    if (product)
        return product;
    else
        throw new Error("Producto no encontrado");
}
exports.getProduct = getProduct;
function addProduct(title, price, thumbnail) {
    if (!(title && price && thumbnail))
        throw new Error("Hay parametros vacios o indefinidos");
    if (Number.isNaN(Number(price)))
        throw new Error("El precio no puede contener caracteres no numericos");
    else if (typeof price !== "number")
        price = parseFloat(price);
    const newProduct = { id: ++currentId, title, price, thumbnail };
    products.push(newProduct);
    try {
        refIo.emit('products', products);
    }
    finally {
        return newProduct;
    }
}
exports.addProduct = addProduct;
function updateProduct(id, title, price, thumbnail) {
    if (!(title && price && thumbnail))
        throw new Error("Hay parametros vacios o indefinidos");
    if (Number.isNaN(Number(id)))
        throw new Error("El id del producto debe ser un caracter numerico");
    else if (typeof id !== "number")
        id = parseInt(id);
    if (Number.isNaN(Number(price)))
        throw new Error("El precio no puede contener caracteres no numericos");
    else if (typeof price !== "number")
        price = parseFloat(price);
    const index = products.findIndex(e => e.id === id);
    if (index !== -1) {
        products[index] = { id, title, price, thumbnail };
        try {
            refIo.emit('products', products);
        }
        finally {
            return products[index];
        }
    }
    throw new Error(`No existe un producto con el id ${id}`);
}
exports.updateProduct = updateProduct;
function removeProduct(id) {
    if (Number.isNaN(Number(id)))
        throw new Error("El id del producto debe ser un caracter numerico");
    else if (typeof id !== "number")
        id = parseInt(id);
    const index = products.findIndex(e => e.id === id);
    if (index !== -1) {
        const aux = products[index];
        products.splice(index, 1);
        try {
            refIo.emit('products', products);
        }
        finally {
            return aux;
        }
    }
    throw new Error(`No existe un producto con el id ${id}`);
}
exports.removeProduct = removeProduct;

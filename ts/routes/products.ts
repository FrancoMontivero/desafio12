import express from 'express';

import { getProducts, getProduct, addProduct, updateProduct, removeProduct } from '../controllers/products';

const routerProducts = express.Router();

routerProducts.get('/listar', (req, res) => {
	try { 
		if(getProducts().length === 0) res.json({error: "No hay productos cargados"});
		else res.json(getProducts()); 
	}
	catch (err) { res.json({"error": err.message}) };
})

routerProducts.get('/listar/:id', (req, res) => {
	try { res.json(getProduct(parseInt(req.params.id))); }
	catch (err) { res.json({"error": err.message}); };
})

routerProducts.post('/guardar', (req, res) => {
	const { title, price, thumbnail } = req.body;
	try { res.json(addProduct(title, price, thumbnail)); }
	catch (err) { res.json({"error": err.message}); };
})

routerProducts.put('/actualizar/:id', (req, res) => {
	const { title, price, thumbnail } = req.body;
	const id: number = parseInt(req.params.id);
	try { res.json(updateProduct(id, title, price, thumbnail)); }
	catch (err) { res.json({ "error": err.message }); };
})

routerProducts.delete('/borrar/:id', (req, res) => {
	const id: number = parseInt(req.params.id);
	try { res.json(removeProduct(id)); }
	catch (err) { res.json({"error": err.message}) }
})

export default routerProducts;
import { type } from 'node:os';
import { Server } from 'socket.io';

export interface Product {
    id?: number,
    title: string,
    price: number,
    thumbnail: string
}

let currentId: number = 0;
let products: Array<Product> = [];
let refIo: Server;

export function socketProducts(io: Server): () => void {
    refIo = io;
    return function () { io.emit('products', products); }
}

export function getProducts(): Array<Product> { return products };

export function getProduct(id: number | string): Object | Product {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const product: Product | undefined = products.find(e => id === e.id);
    if(product) return product
    else throw new Error("Producto no encontrado");
}

export function addProduct(title: string, price: string | number, thumbnail: string) {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 

    const newProduct = { id: ++currentId, title, price, thumbnail };
    products.push(newProduct);
    try { refIo.emit('products', products); }
    finally { return newProduct }
}

export function updateProduct(id: number | string, title: string, price: number, thumbnail: string): Object {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico");
    else if(typeof id !== "number") id = parseInt(id);
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 
    const index: number = products.findIndex(e => e.id === id);
    if(index !== -1) {
        products[index] = { id, title, price, thumbnail }
        try { refIo.emit('products', products); }
        finally { return products[index]; }
    }
    throw new Error(`No existe un producto con el id ${id}`)
}

export function removeProduct(id: number | string) {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const index: number = products.findIndex(e => e.id === id);
    if(index !== -1) {
        const aux :Product = products[index];
        products.splice(index, 1);
        try { refIo.emit('products', products); }
        finally { return aux; }
    }
    throw new Error(`No existe un producto con el id ${id}`)
}